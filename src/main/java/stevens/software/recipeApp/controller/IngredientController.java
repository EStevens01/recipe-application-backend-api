package stevens.software.recipeApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.service.IngredientService;

import java.util.List;

@RestController
public class IngredientController {

    @Autowired
    private IngredientService ingredientService;

    @GetMapping("/ingredients")
    public List<Ingredient> getIngredients(){
        return ingredientService.getIngredients();
    }

    @PostMapping("/ingredient")
    public void createIngredient(@RequestBody Ingredient ingredient){
        ingredientService.createIngredient(ingredient);
    }

}
