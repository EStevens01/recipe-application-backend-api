package stevens.software.recipeApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import stevens.software.recipeApp.domain.Recipe;
import stevens.software.recipeApp.service.RecipeService;

import java.util.List;

@RestController
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @GetMapping("/recipes")
    public List<Recipe> getRecipes(){
        return recipeService.getAllRecipes();
    }

    @PostMapping("/recipe")
    public void createRecipe(@RequestBody Recipe recipe){
        recipeService.createRecipe(recipe);
    }

    @DeleteMapping("/recipe/{recipeId}")
    public void deleteRecipe(@PathVariable Integer recipeId){
        recipeService.deleteRecipe(recipeId);
    }

    @GetMapping("/recipe/{recipeId}")
    public Recipe findRecipeById(@PathVariable Integer recipeId){
        return recipeService.findRecipeById(recipeId);
    }

    @PutMapping("/recipe/{recipeId}")
    public Recipe updateRecipe(@PathVariable Integer recipeId, @RequestBody Recipe recipe){
        return recipeService.updateRecipe(recipeId, recipe);
    }
}
