package stevens.software.recipeApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.service.MethodService;

@RestController
public class MethodController {

    @Autowired
    private MethodService methodService;

    @PostMapping("/method")
    public void createIngredient(@RequestBody Method method){
        methodService.createMethod(method);
    }
}
