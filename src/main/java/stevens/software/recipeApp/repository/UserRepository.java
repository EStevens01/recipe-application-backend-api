package stevens.software.recipeApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import stevens.software.recipeApp.domain.ApplicationUser;

public interface UserRepository extends JpaRepository<ApplicationUser, Integer> {
    ApplicationUser findByUsername(String username);
}
