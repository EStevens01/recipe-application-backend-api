package stevens.software.recipeApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.domain.Recipe;

public interface IngredientRepository extends JpaRepository<Ingredient, Integer> {
}
