package stevens.software.recipeApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.domain.Recipe;

public interface MethodRepository extends JpaRepository<Method, Integer> {
}
