package stevens.software.recipeApp.domain;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
//@Builder
@Entity
@Table(name = "recipes")
public class Recipe {

    @Id
    @GeneratedValue
    private Integer recipeId;

    @Column(name = "name")
    private String recipeName;

    @ManyToMany
    @JoinTable(
            name = "recipe_ingredients",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "ingredient_id"))
    private Set<Ingredient> ingredients;

    @OneToMany
    private Set<Method> method;
}
