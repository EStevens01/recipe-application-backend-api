package stevens.software.recipeApp.domain;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ingredients")
public class Ingredient {

    @Id
    @GeneratedValue
    private Integer ingredientId;

    @Column(name = "ingredient")
    private String ingredient;
}
