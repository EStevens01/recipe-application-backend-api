package stevens.software.recipeApp.domain;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "method")
public class Method {

    @Id
    @GeneratedValue
    private Integer methodId;

    @Column(name = "stepNumber")
    private Integer stepNumber;

    @Column(name = "step")
    private String step;

//    @ManyToOne
//    @JoinColumn(name="recipe_id")
//    private Recipe recipe;
}
