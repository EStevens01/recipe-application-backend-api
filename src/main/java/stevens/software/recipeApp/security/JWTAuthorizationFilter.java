package stevens.software.recipeApp.security;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;

import static stevens.software.recipeApp.security.SecurityConstants.*;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager){
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_STRING);

        if(header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authenticationToken = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request){
        String token = request.getHeader(HEADER_STRING);
        String tokenm = token.replace("Bearer ", "");

//        if (token != null) {
//            String user = JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
//                    .build()
//                    .verify(tokenm)
//                    .getSubject();
//
//            if (user != null) {
//                return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
//            }
//            return null;
//        }


        if (token != null) {
            try {
                DecodedJWT jwt = JWT.decode(tokenm);
                JwkProvider provider = new UrlJwkProvider("https://dev-mq4txwco.us.auth0.com/");
                Jwk jwk = provider.get(jwt.getKeyId());

                RSAPublicKey publicKey = (RSAPublicKey) jwk.getPublicKey();
                try {
                    Algorithm algorithm = Algorithm.RSA256(publicKey, null);
                    JWTVerifier verifier = JWT.require(algorithm)
                            .withIssuer("https://dev-mq4txwco.us.auth0.com/")
                            .build(); //Reusable verifier instance
                    DecodedJWT jwtt = verifier.verify(tokenm);
                    System.out.println("hola " + jwtt.getSubject());

                return new UsernamePasswordAuthenticationToken(jwtt, null, new ArrayList<>());

                } catch (JWTVerificationException exception) {
                    //Invalid signature/claims
                    System.out.println(exception);
                    System.out.println("crymore");
                }
            } catch (JWTDecodeException | JwkException exception){
                System.out.println(exception);
                System.out.println("cry");
            }


            return null;
        }







        return null;
    }
}
