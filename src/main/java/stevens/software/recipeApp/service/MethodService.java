package stevens.software.recipeApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.repository.MethodRepository;

@Service
public class MethodService {

    @Autowired
    private MethodRepository methodRepository;

    public Method createMethod(Method method){
        return methodRepository.save(method);
    }

}
