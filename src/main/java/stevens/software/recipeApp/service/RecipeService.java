package stevens.software.recipeApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.domain.Recipe;
import stevens.software.recipeApp.repository.IngredientRepository;
import stevens.software.recipeApp.repository.MethodRepository;
import stevens.software.recipeApp.repository.RecipeRepository;

import javax.print.DocFlavor;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired MethodRepository methodRepository;

    public Recipe createRecipe(Recipe recipe){

        if(recipe.getIngredients() != null){
            for(Ingredient ingredient : recipe.getIngredients()){
                ingredientRepository.save(ingredient);
            }
        }
        if(recipe.getMethod() != null){
            for(Method method : recipe.getMethod()){
                methodRepository.save(method);
            }
        }
        return recipeRepository.save(recipe);

    }

    public List<Recipe> getAllRecipes(){
        return recipeRepository.findAll();
    }

    public void deleteRecipe(Integer recipeId){
        recipeRepository.deleteById(recipeId);
    }

    public Recipe findRecipeById(Integer recipeId){
        Optional<Recipe> recipe = recipeRepository.findById(recipeId);
        return recipe.orElse(null);
    }

    public Recipe updateRecipe(Integer recipeId, Recipe recipe){
        if (recipeRepository.findById(recipeId).isPresent()) {
            Recipe updatedRecipe = recipeRepository.findById(recipeId).get();

            updatedRecipe.setRecipeName(recipe.getRecipeName());
            return recipeRepository.save(updatedRecipe);
        }else{
            if(recipe.getIngredients() != null){
                for(Ingredient ingredient : recipe.getIngredients()){
                    ingredientRepository.save(ingredient);
                }
            }
            return recipeRepository.save(recipe);
        }

    }
}
