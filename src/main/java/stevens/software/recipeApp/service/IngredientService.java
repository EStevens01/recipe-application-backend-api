package stevens.software.recipeApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.domain.Recipe;
import stevens.software.recipeApp.repository.IngredientRepository;

import java.util.List;

@Service
public class IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    public List<Ingredient> getIngredients(){
        return ingredientRepository.findAll();
    }

    public Ingredient createIngredient(Ingredient ingredient) {
        if (ingredientRepository.findById(ingredient.getIngredientId()).isPresent()) {
            return null;
        }
        return ingredientRepository.save(ingredient);
    }
}
