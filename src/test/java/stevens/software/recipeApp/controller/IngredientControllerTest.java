package stevens.software.recipeApp.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.ContentResultMatchers;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.repository.IngredientRepository;
import stevens.software.recipeApp.service.IngredientService;
import stevens.software.recipeApp.util.TestUtils;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static stevens.software.recipeApp.util.TestUtils.convertToJsonString;
import static stevens.software.recipeApp.util.TestUtils.getIngredient;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class IngredientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IngredientService ingredientService;

    @MockBean
    private IngredientRepository ingredientRepository;


    @Test
    public void testGetIngredients() throws Exception {
        List<Ingredient> expected = TestUtils.getIngredients();
        when(ingredientService.getIngredients()).thenReturn(expected);
        mockMvc.perform(get("/ingredients")).andDo(print()).andExpect(status().isOk())
            .andExpect(content().json(convertToJsonString(expected)));
    }

    @Test
    public void testCreateIngredients() throws Exception {
        Ingredient expected = getIngredient();
        when(ingredientService.createIngredient(getIngredient())).thenReturn(getIngredient());
        mockMvc.perform(post("/ingredient")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertToJsonString(expected))).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(convertToJsonString(expected)));
    }
}

