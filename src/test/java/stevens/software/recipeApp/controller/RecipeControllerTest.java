package stevens.software.recipeApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import stevens.software.recipeApp.domain.Recipe;
import stevens.software.recipeApp.repository.RecipeRepository;
import stevens.software.recipeApp.service.RecipeService;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static stevens.software.recipeApp.util.TestUtils.*;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class RecipeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecipeService recipeService;

    @MockBean
    private RecipeRepository recipeRepository;

    @Test
    public void testGetRecipes() throws Exception {
        List<Recipe> expected = getRecipes();
        when(recipeService.getAllRecipes()).thenReturn(getRecipes());
        mockMvc.perform(get("/recipe/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(convertToJsonString(expected)));
    }

    @Test
    public void testGetRecipe() throws Exception {
        Recipe expected = getRecipe();
        when(recipeService.findRecipeById(1)).thenReturn(getRecipe());
        mockMvc.perform(get("/recipe/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(convertToJsonString(expected)));
    }

    @Test
    public void testCreateRecipe() throws Exception {
        Recipe expected = getRecipe();
        when(recipeService.createRecipe(getRecipe())).thenReturn(getRecipe());
        mockMvc.perform(post("/recipe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertToJsonString(expected))).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(convertToJsonString(expected)));
    }

    @Test
    public void testUpdateRecipe() throws Exception {
        Recipe expected = getRecipe();
        when(recipeService.updateRecipe(1, getRecipe())).thenReturn(getRecipe());
        mockMvc.perform(put("/recipe/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertToJsonString(expected))).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(convertToJsonString(expected)));
    }
}