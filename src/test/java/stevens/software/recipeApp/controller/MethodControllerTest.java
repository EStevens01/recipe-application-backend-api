package stevens.software.recipeApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.repository.IngredientRepository;
import stevens.software.recipeApp.repository.MethodRepository;
import stevens.software.recipeApp.service.IngredientService;
import stevens.software.recipeApp.service.MethodService;
import stevens.software.recipeApp.util.TestUtils;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static stevens.software.recipeApp.util.TestUtils.*;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class MethodControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MethodService methodService;

    @MockBean
    private MethodRepository methodRepository;

    @Test
    public void testCreateMethod() throws Exception {
        Method expected = getMethod();
        when(methodService.createMethod(getMethod())).thenReturn(getMethod());
        mockMvc.perform(post("/method")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertToJsonString(expected))).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(convertToJsonString(expected)));
    }
}