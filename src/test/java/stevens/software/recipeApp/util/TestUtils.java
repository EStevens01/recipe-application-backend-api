package stevens.software.recipeApp.util;

import stevens.software.recipeApp.domain.Ingredient;

import java.util.Collections;
import java.util.List;
import com.google.gson.Gson;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.domain.Recipe;


public class TestUtils {

    public static String convertToJsonString(Object object) {
        Gson gson = new Gson();
        String hi = gson.toJson(object);
        System.out.println("hey " + hi);
        return gson.toJson(object);
    }

    public static List<Ingredient> getIngredients() {
        Ingredient ingredient = new Ingredient();
        ingredient.setIngredientId(1);
        ingredient.setIngredient("Tomato");
        return Collections.singletonList(ingredient);
    }

    public static Ingredient getIngredient() {
        Ingredient ingredient = new Ingredient();
        ingredient.setIngredientId(1);
        ingredient.setIngredient("Tomato");
        return ingredient;
    }

    public static List<Recipe> getRecipes() {
        Ingredient ingredient = new Ingredient();
        ingredient.setIngredientId(1);
        ingredient.setIngredient("Tomato");

        Method method = new Method();
        method.setMethodId(1);
        method.setStepNumber(1);
        method.setStep("Turn on oven");

        Recipe recipe = new Recipe();
        recipe.setIngredients(Collections.singleton(ingredient));
        recipe.setMethod(Collections.singleton(method));
        recipe.setRecipeName("Pasta dish");
        return Collections.singletonList(recipe);
    }

    public static Recipe getRecipe() {
        Ingredient ingredient = new Ingredient();
        ingredient.setIngredientId(1);
        ingredient.setIngredient("Tomato");

        Method method = new Method();
        method.setMethodId(1);
        method.setStepNumber(1);
        method.setStep("Turn on oven");

        Recipe recipe = new Recipe();
        recipe.setIngredients(Collections.singleton(ingredient));
        recipe.setMethod(Collections.singleton(method));
        recipe.setRecipeName("Pasta dish");
        return recipe;
    }

    public static Method getMethod() {
        Method method = new Method();
        method.setMethodId(1);
        method.setStepNumber(1);
        method.setStep("Turn on oven");
        return method;
    }

}
