package stevens.software.recipeApp.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.domain.Recipe;
import stevens.software.recipeApp.repository.IngredientRepository;
import stevens.software.recipeApp.repository.MethodRepository;
import stevens.software.recipeApp.repository.RecipeRepository;
import stevens.software.recipeApp.util.TestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RecipeServiceTest {

    @Mock
    private RecipeRepository recipeRepository;

    @Mock
    private MethodRepository methodRepository;

    @Mock
    private IngredientRepository ingredientRepository;

    @InjectMocks
    private RecipeService recipeService;

    @Test
    public void testCreateRecipe() {
        Recipe expected = TestUtils.getRecipe();
        when(recipeRepository.save(TestUtils.getRecipe()))
                .thenReturn(TestUtils.getRecipe());
        when(ingredientRepository.save(TestUtils.getIngredient())).thenReturn(TestUtils.getIngredient());
        when(methodRepository.save(TestUtils.getMethod())).thenReturn(TestUtils.getMethod());
        Recipe actual = recipeService.createRecipe(TestUtils.getRecipe());
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAllRecipes() {
        List<Recipe> expected = TestUtils.getRecipes();
        when(recipeRepository.findAll())
                .thenReturn(TestUtils.getRecipes());
        List<Recipe> actual = recipeService.getAllRecipes();
        assertEquals(expected, actual);
    }

    @Test
    public void testUpdateRecipe() {
        Recipe expected = TestUtils.getRecipe();
        when(recipeRepository.findById(1))
                .thenReturn(java.util.Optional.of(expected));
        when(recipeRepository.save(expected)).thenReturn(expected);
        Recipe actual = recipeService.updateRecipe(1, TestUtils.getRecipe());
        assertEquals(expected, actual);
    }

    @Test
    public void testFindRecipeById() {
        Recipe expected = TestUtils.getRecipe();
        when(recipeRepository.findById(1))
                .thenReturn(java.util.Optional.of(expected));
        Recipe actual = recipeService.findRecipeById(1);
        assertEquals(expected, actual);
    }
}