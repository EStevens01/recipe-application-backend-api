package stevens.software.recipeApp.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.repository.IngredientRepository;
import stevens.software.recipeApp.util.TestUtils;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IngredientServiceTest {

    @Mock
    private IngredientRepository ingredientRepository;

    @InjectMocks
    private IngredientService ingredientService;

    @Test
    public void testGetAllIngredients() {
        List<Ingredient> expected = TestUtils.getIngredients();
        when(ingredientRepository.findAll())
                .thenReturn(TestUtils.getIngredients());
        List<Ingredient> actual = ingredientService.getIngredients();
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateIngredient() {
        Ingredient expected = TestUtils.getIngredient();
        when(ingredientRepository.save(TestUtils.getIngredient()))
                .thenReturn(TestUtils.getIngredient());
        Ingredient actual = ingredientService.createIngredient(TestUtils.getIngredient());
        assertEquals(expected, actual);
    }
}