package stevens.software.recipeApp.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.recipeApp.domain.Ingredient;
import stevens.software.recipeApp.domain.Method;
import stevens.software.recipeApp.repository.IngredientRepository;
import stevens.software.recipeApp.repository.MethodRepository;
import stevens.software.recipeApp.util.TestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MethodServiceTest {

    @Mock
    private MethodRepository methodRepository;

    @InjectMocks
    private MethodService methodService;

    @Test
    public void testCreateMethod() {
        Method expected = TestUtils.getMethod();
        when(methodRepository.save(TestUtils.getMethod()))
                .thenReturn(TestUtils.getMethod());
        Method actual = methodService.createMethod(TestUtils.getMethod());
        assertEquals(expected, actual);
    }
}